package cn.it.ShopSystem.service.impl;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import net.sf.json.JSONSerializer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.it.ShopSystem.model.Product;
import cn.it.ShopSystem.service.ProductService;
import cn.it.ShopSystem.service.SorderService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext-*.xml")
public class ProductServiceImplTest {
	@Resource
	private ProductService productService;
	@Resource
	private SorderService sorderService;
	@Test
	public void querySale() {
		System.out.println(JSONSerializer.toJSON(sorderService.querySale(5)));
//		for (Object temp : sorderService.querySale(5)) {
//			System.out.println(temp);
//		}
	}
	@Test
	public void testQueryJoinCategory() {
		for(Product temp : productService.queryJoinCategory("",1,5)){
			temp.getCategory().setAccount(null);
			System.out.println(temp+"/"+JSONSerializer.toJSON(temp));
		}
	}

	@Test
	public void query() {
		System.out.println(productService.query());
	}
	
	@Test
	public void testGetCount() {
		System.out.println(productService.getCount(""));
	}
	
	@Test
	public void testByCid() {
		for(Product temp : productService.queryByCid(1)){
			System.out.println(temp);
		}
	}

}
