package cn.it.ShopSystem.service.impl;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.it.ShopSystem.model.Category;
import cn.it.ShopSystem.service.CategoryService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext-*.xml")
public class CategoryServiceImplTest {

	@Resource
	private CategoryService categoryService;
	
	@Test
	public void testQueryJoinAccount() {
		for(Category temp : categoryService.queryJoinAccount("",1,2)){
			System.out.println(temp+"|"+temp.getAccount());
		}
	}
	
	@Test
	public void getCount() {
		System.out.println(categoryService.getCount(""));
	}
	@Test
	public void deleteByIds() {
		categoryService.deleteByIds("1,2");
	}
}
