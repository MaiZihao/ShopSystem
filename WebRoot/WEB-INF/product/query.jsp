<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  	<%@ include file="/public/head.jspf" %>
  	<style type="text/css">
  		body{
  			margin: 1px;
  		}
  	</style>
    <script type="text/javascript">
    	$(function(){
    		$('#dg').datagrid({    
    			//请求数据的url地址
			    url:'product_queryJoinCategory.action', 
			    //在从远程站点加载数据的时候显示提示消息。
			    loadMsg:'请等待.......',
			    //width:500,
			    //指明哪一个字段是标识字段。在删除、更新的时候有用，如果配置成此字段，在翻页的时候被选中的记录是不会丢失的
			    idField:'id',
			    //是否显示斑马线效果。
			    striped:true,
			    //如果为true，则在同一行中显示数据。设置为true可以提高加载性能。 
			    nowrap:true,
			    //真正的自动展开/收缩列的大小，以适应网格的宽度，防止水平滚动。
			    fitColumns:true,
			    //返回样式如'background:red'。带2个参数的函数：index：该行索引从0开始  row：与此相对应的记录行。 
			    /*rowStyler: function(index,row){
					if (index%2==0){
						return 'background-color:#999;';
					}else{
						return 'background-color:#666;';
					}
				},*/
				//单行显示,全选功能失效
				//singleSelect:true,
				//如果为true，则在DataGrid控件底部显示分页工具栏。
				pagination:true,
				//在设置分页属性的时候初始化页面大小。
			    pageSize:15,
			    pageList:[5,10,15,20,25],
				//在请求远程数据的时候发送额外的参数。 	
				queryParams:{name:''},
				//顶部工具栏的DataGrid面板。
				toolbar: [{
					iconCls: 'icon-add',
					text:'添加商品',
					handler: function(){
						parent.$("#win").window({
							title:'添加商品',
							width:500,
							height:600,
	  						content:'<iframe src="send_product_save.action" frameborder="0" width="100%" height="100%" />',
						
						});
					}
				},'-',{
					iconCls: 'icon-edit',
					text:'更新商品',
					handler: function(){
						alert("--自己实现--");
					}
				},'-',{
					iconCls: 'icon-remove',
					text:'删除商品',
					handler: function(){
						alert("--自己实现--");
					}
				},'-',{
					text:"<input id='ss' name='search'/>",
				}],
									    
			    //同列属性，但是这些列将会被冻结在左侧。
			    frozenColumns:[[
			    	{field:'check',checkbox:true},
			    	{field:'id',title:'商品编号',width:100,align:'center'},
			    ]],
			    
			    //配置dg的列字段，  field：列字段名称，
			    columns:[[     
			        {field:'name',title:'商品名称',width:200,align:'center'},    
			        {field:'remark',title:'简单介绍',width:300,align:'center'},
			        {field:'category.type',title:'所属类别',width:200,align:'center',
			        	formatter: function(value,row,index){
			        		if(row.category != null && row.category.type != null){
			        			return row.category.type;
			        		}							
						}
			        },  
			    ]]    
			});
			//把普通文本框转换成搜索文本框
			$('#ss').searchbox({ 
				//触发查询事件
				searcher:function(value,name){ 
					//获取当前查询的关键字，通过id加载相应的信息
					//alert(value + "," + name); 
					$('#dg').datagrid('load',{name:value});
					
										
				}, 
				prompt:'请输入查询关键字' 
			});
			 
    	});
    </script>	
  </head>
  
  <body>
  	<table id="dg"></table>
  </body>
</html>
