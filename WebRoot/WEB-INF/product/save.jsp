<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<!-- 引入公共的JSP片段 -->
	<%@ include file="/public/head.jspf"%>
	<style type="text/css">
		form div{
			margin: 10px;
		}
	</style>
	<script type="text/javascript">
		//自定义验证方法(注册新函数)
		$.extend($.fn.validatebox.defaults.rules, {  
			//函数的名称：函数的实现体  (又是一个josn对象，里面包含函数的实现和错误信息设置)
		    format: {  
		    	//函数实现，如果返回为false，则验证失败  
		        validator: function(value,param){ 
		           //获取当前文件的后缀名
		           var ext = value.substring(value.lastIndexOf('.')+1);
		           //获取支持的文件后缀名，然后进行比较
		           var arr = param[0].split(",");
		           for(var i = 0 ;i < arr.length;i++){
		           		if(ext == arr[i]){
		           			return true;
		           		}
		           }
		            return false;
		        }, 
	        //错误信息   
	        //message: '文件格式必须为:{0}'  
	        message: '文件格式不正确'    
		    }    
		});  
			
		
				
		$(function(){
			$("input[name=name]").validatebox({
  				required:true,
  				missingMessage:'请输入商品名称'
  				 	
  			});
  			$("input[name=price]").numberbox({
  				required:true,
  				missingMessage:'请输入商品价格',
  				min:0,    
    			precision:2,
    			prefix:'$'  	
  			});
  			$("input[name='fileImage.upload']").validatebox({
  				required:true,
  				missingMessage:'请上传商品图片',
  				//设置自定义方法
  				validType:"format['gif,jpg,jpeg,png']" 	
  			});
  			$("textarea[name=remark]").validatebox({
  				required:true,
  				missingMessage:'不能为空'
  				 	
  			});
  			//对管理员的下拉列表框进行远程加载
			$('#cc').combobox({    
			    url:'category_query.action',    
			    valueField:'id',    
			    textField:'type',
			    panelHeight:'auto',
			    panelWidth:180,
			    width:180,
			    editable:false,
			    required:true,
  				missingMessage:'请选择所属类别'
			}); 
			//窗体弹出默认禁用验证
  			$("#ff").form("disableValidation");
  			//注册button的事件
  			$("#submit").click(function(){
				//开启验证
				$("#ff").form("enableValidation");
				//如果验证成功，则提交数据
				if($("#ff").form("validate")){
					//调用submit方法，提交数据
					$('#ff').form('submit', {
						url: 'product_save.action',
						success: function(){
							//关闭当前页面
							parent.$("#win").window("close");
							//刷新页面  获取 aindex-->iframe-->dg   dom-->jquery-->easyui(兼容性问题，向下转化)
							//parent.$("iframe[title=类别管理]").contents().find("#dg").datagrid("reload");
							parent.$("iframe[title=类别管理]").get(0).contentWindow.$("#dg").datagrid("reload");
						}
					});										
				}			
			});
			//数据重置
			$("#reset").click(function(){
				$("#ff").form("reset");
			});
		});
	</script>
</head>

<body>
<form title="添加商品" id="ff" method="post" enctype="multipart/form-data">
	<div>
		<label>商品名称:</label> <input type="text" name="name" />
	</div>
	<div>
		<label>商品价格:</label> <input type="text" name="price" />
	</div>
	<div>
		<label>图片上传:</label> <input type="file" name="fileImage.upload" />
	</div>
	<div>
		<label>所属类别：</label> 
		<!-- 适于远程加载数据 -->
		<input id="cc" name="category.id" />		
	</div>
	<div>
		<label>加入推荐:</label> 
			推荐:<input type="radio" name="commend"
			checked="checked" value="true" />  
			不推荐:<input type="radio"
			name="commend" value="false" /> 
	</div>
	<div>
		<label>是否有效:</label>
		上架:<input type="radio" name="open" checked="checked"value="true" />
		下架:<input type="radio" name="open" value="false" />
				
	</div>
	<div>
		<label>简单描述:</label>
		<textarea name="remark" cols="40" rows="4"></textarea>
	</div>
	<div>
		<label>详细描述:</label>
		<textarea name="xremark" cols="40" rows="8"></textarea>
	</div>
	<div>
		<a id="submit" href="#" class="easyui-linkbutton">添 加</a> 
		<a id="reset" href="#" class="easyui-linkbutton">重 置</a>
	</div>
</form>
</body>
</html>
