<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  	<%@ include file="/public/head.jspf" %>
  	<style type="text/css">
  		body{
  			margin: 1px;
  		}
  	</style>
    <script type="text/javascript">
    	$(function(){
    		$('#dg').datagrid({    
    			//请求数据的url地址
			    url:'category_queryJoinAccount.action', 
			    //在从远程站点加载数据的时候显示提示消息。
			    loadMsg:'请等待.......',
			    //width:500,
			    //指明哪一个字段是标识字段。在删除、更新的时候有用，如果配置成此字段，在翻页的时候被选中的记录是不会丢失的
			    idField:'id',
			    //是否显示斑马线效果。
			    striped:true,
			    //如果为true，则在同一行中显示数据。设置为true可以提高加载性能。 
			    nowrap:true,
			    //真正的自动展开/收缩列的大小，以适应网格的宽度，防止水平滚动。
			    fitColumns:true,
			    //返回样式如'background:red'。带2个参数的函数：index：该行索引从0开始  row：与此相对应的记录行。 
			    /*rowStyler: function(index,row){
					if (index%2==0){
						return 'background-color:#999;';
					}else{
						return 'background-color:#666;';
					}
				},*/
				//单行显示,全选功能失效
				//singleSelect:true,
				//如果为true，则在DataGrid控件底部显示分页工具栏。
				pagination:true,
				//在设置分页属性的时候初始化页面大小。
			    pageSize:15,
			    pageList:[5,10,15,20,25],
				//在请求远程数据的时候发送额外的参数。 	
				queryParams:{type:''},
				//顶部工具栏的DataGrid面板。
				toolbar: [{
					iconCls: 'icon-add',
					text:'添加类别',
					handler: function(){
						parent.$("#win").window({
							title:'添加类别',
							width:320,
							height:180,
	  						content:'<iframe src="send_category_save.action" frameborder="0" width="100%" height="100%" />',
						
						});

					}
				},'-',{
					iconCls: 'icon-edit',
					text:'更新类别',
					handler: function(){
						//1、判断是否有选中的记录
						var rows = $("#dg").datagrid("getSelections");	
						//2、rows 返回被选中的行，没有任何选中则返回空数组
						if(rows.length!=1){
							//弹出提示信息
							$.messager.show({
								title:'错误提示',
								msg:'只能更新一条记录',
								timeout:2000,
								showType:'slide'
							});
						}else{
							//1、弹出更新页面
							parent.$("#win").window({
								title:'更新类别',
								width:360,
								height:250,
		  						content:'<iframe src="send_category_update.action" frameborder="0" width="100%" height="100%" />',
						
							});
						}
					}
				},'-',{
					iconCls: 'icon-remove',
					text:'删除类别',
					handler: function(){
						//1、判断是否有选中的记录
						var rows = $("#dg").datagrid("getSelections");	
						//2、rows 返回被选中的行，没有任何选中则返回空数组
						if(rows.length==0){
							//弹出提示信息
							$.messager.show({
								title:'错误提示',
								msg:'至少选择一条记录',
								timeout:2000,
								showType:'slide'
							});
						}else{
							//提示是否确认删除，如果确认则执行删除的逻辑
							$.messager.confirm('删除对话框', '是否要删除选中的记录', function(r){
								if (r){
								    //1、获取被选中的记录，然后从记录中获取相应的id
								    var ids = "";
								    for ( var i = 0; i < rows.length; i++) {
										ids += rows[i].id + ",";
									}								
								    //2、拼接id的值，然后发送给后台
								    ids =ids.substring(0,ids.lastIndexOf(","));
								    //3、发送ajax请求
								    $.post("category_deleteByIds.action",{ids:ids},function(result){
								    	//
								    	if(result == "true"){
								    		//取消选中的所有行
								    		$("#dg").datagrid("uncheckAll");
								    		//重新刷新当前页面
								    		$("#dg").datagrid("reload");
								    	}else{
								    		//弹出提示信息
											$.messager.show({
												title:'删除异常',
												msg:'删除失败，请检查操作',
												timeout:2000,
												showType:'slide'
											});
								    	}
								    },"text");
								}
							});													
						}
					
					}
				},'-',{
					text:"<input id='ss' name='search'/>",
				}],
									    
			    //同列属性，但是这些列将会被冻结在左侧。
			    frozenColumns:[[
			    	{field:'check',checkbox:true},
			    	{field:'id',title:'编号',width:100,align:'center'},
			    ]],
			    
			    //配置dg的列字段，  field：列字段名称，
			    columns:[[     
			        {field:'type',title:'类别名称',width:100,align:'center',
			        	//单元格formatter(格式化器)函数，返回最终的数据，带3个参数：value：字段值。row：行记录数据。index: 行索引
			        	formatter: function(value,row,index){
							return "<span title="+ value +">"+value+"</span>";
						}
						        	
			        },    
			        {field:'hot',title:'热点',width:100,align:'center',
			        	formatter: function(value,row,index){
							if(value){
								return "<input type='checkbox' checked='checked' disabled='true' />";
							}else{
								return "<input type='checkbox' disabled='true' />";
							}
						}
			        },
			        {field:'account.login',title:'所属管理',width:100,align:'center',
			        	formatter: function(value,row,index){
			        		if(row.account != null && row.account.login != null){
			        			return row.account.login;
			        		}							
						}
			        },  
			    ]]    
			});
			//把普通文本框转换成搜索文本框
			$('#ss').searchbox({ 
				//触发查询事件
				searcher:function(value,name){ 
					//获取当前查询的关键字，通过id加载相应的信息
					//alert(value + "," + name); 
					$('#dg').datagrid('load',{type:value});
					
										
				}, 
				prompt:'请输入查询关键字' 
			});
			 
    	});
    </script>	
  </head>
  
  <body>
  	<table id="dg"></table>
  </body>
</html>
