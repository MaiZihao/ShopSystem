package cn.it.ShopSystem.dao;


import java.math.BigDecimal;

import cn.it.ShopSystem.model.Forder;

public interface ForderDao  extends BaseDao<Forder>{
	
	//根据订单编号更新订单状态
	public void updateStatusById(int id , int sid);
}
