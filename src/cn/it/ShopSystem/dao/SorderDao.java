package cn.it.ShopSystem.dao;


import java.util.List;

import cn.it.ShopSystem.model.Forder;
import cn.it.ShopSystem.model.Product;
import cn.it.ShopSystem.model.Sorder;

public interface SorderDao  extends BaseDao<Sorder>{
	
	//查询热点商品的销售量
	public List<Object> querySale(int number);
}
