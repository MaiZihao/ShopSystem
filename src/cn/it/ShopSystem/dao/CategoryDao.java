package cn.it.ShopSystem.dao;

import java.util.List;

import cn.it.ShopSystem.model.Category;

public interface CategoryDao  extends BaseDao<Category>{
	//查询类别信息，级联管理员--根据关键字和分页的参数查询相应的数据
	public List<Category> queryJoinAccount(String type,int page,int size);
	//根据关键字查询总记录数
	public Long getCount(String type);
	
	//根据id删除多条记录
	public void deleteByIds(String ids);
	
	//根据boolean查询热点或者非热点类别查询
	public List<Category> queryByHot(boolean hot);
}
