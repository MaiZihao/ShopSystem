package cn.it.ShopSystem.dao;

import cn.it.ShopSystem.model.User;

public interface UserDao  extends BaseDao<User>{

	public User login(User user);
}
