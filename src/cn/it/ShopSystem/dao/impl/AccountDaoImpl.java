package cn.it.ShopSystem.dao.impl;

import org.springframework.stereotype.Repository;

import cn.it.ShopSystem.dao.AccountDao;
import cn.it.ShopSystem.model.Account;
/**
 * 模块自身的业务逻辑
 * @author 麦子
 *
 */
@Repository("accountDao")
public class AccountDaoImpl extends BaseDaoImpl<Account> implements AccountDao {
	

}
