package cn.it.ShopSystem.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.it.ShopSystem.dao.CategoryDao;
import cn.it.ShopSystem.model.Category;
/**
 * 模块自身的业务逻辑
 * @author 麦子
 *
 */
@SuppressWarnings("unchecked")
@Repository("categoryDao")
public class CategoryDaoImpl extends BaseDaoImpl<Category> implements CategoryDao {
	public CategoryDaoImpl(){
		super();
	}

	@Override
	public List<Category> queryJoinAccount(String type,int page,int size) {
		String hql = "from Category c LEFT join FETCH c.account where c.type like :type";
		return getSession().createQuery(hql)
				.setString("type","%" + type + "%")
				.setFirstResult((page-1)*size)
				.setMaxResults(size)
				.list();
	}

	@Override
	public Long getCount(String type) {
		String hql = "select count(c) from Category c where c.type like :type";
		return (Long)getSession().createQuery(hql)
				.setString("type","%"+ type+"%")
				.uniqueResult();
		
	}

	@Override
	public void deleteByIds(String ids) {
		String hql = "delete from Category where id in (" + ids + ")";
		getSession().createQuery(hql)
		.executeUpdate();
	}

	@Override
	public List<Category> queryByHot(boolean hot) {
		String hql = "from Category c where c.hot =:hot";
		return getSession().createQuery(hql)
		.setBoolean("hot", hot)
		.list();
	}

}
