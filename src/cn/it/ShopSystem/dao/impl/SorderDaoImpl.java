package cn.it.ShopSystem.dao.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import cn.it.ShopSystem.dao.SorderDao;
import cn.it.ShopSystem.model.Forder;
import cn.it.ShopSystem.model.Product;
import cn.it.ShopSystem.model.Sorder;
@Repository("sorderDao")
@SuppressWarnings("unchecked")
public class SorderDaoImpl extends BaseDaoImpl<Sorder> implements
		SorderDao {
	@Override
	public List<Object> querySale(int number) {
		String hql = "select s.name,sum(s.number) from Sorder s join s.product group by s.product.id";
		return getSession().createQuery(hql)
		.setFirstResult(0)
		.setMaxResults(number)
		.list();
	}

}
