package cn.it.ShopSystem.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.it.ShopSystem.dao.ProductDao;
import cn.it.ShopSystem.model.Product;
@SuppressWarnings("unchecked")
@Repository("productDao")
public class ProductDaoImpl extends BaseDaoImpl<Product> implements
		ProductDao {

	@Override
	public List<Product> queryJoinCategory(String name, int page, int size) {
		String hql = "from Product p LEFT join FETCH p.category where p.name like :name";
		return getSession().createQuery(hql)
				.setString("name","%" + name + "%")
				.setFirstResult((page-1)*size)
				.setMaxResults(size)
				.list();
	}

	@Override
	public Long getCount(String name) {
		String hql = "select count(p) from Product p where p.name like :name";
		return (Long)getSession().createQuery(hql)
				.setString("name","%"+ name+"%")
				.uniqueResult();
	}

	@Override
	public List<Product> queryByCid(int cid) {
		String hql = "from Product p join FETCH p.category where p.commend=true and p.open=true and p.category.id=:cid order by p.date desc";
		return getSession().createQuery(hql)
				.setInteger("cid",cid)
				.setFirstResult(0)
				.setMaxResults(4)
				.list();
	}
	
}
