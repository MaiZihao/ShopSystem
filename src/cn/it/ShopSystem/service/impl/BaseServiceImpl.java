package cn.it.ShopSystem.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import cn.it.ShopSystem.dao.AccountDao;
import cn.it.ShopSystem.dao.BaseDao;
import cn.it.ShopSystem.dao.CategoryDao;
import cn.it.ShopSystem.dao.ForderDao;
import cn.it.ShopSystem.dao.ProductDao;
import cn.it.ShopSystem.dao.SorderDao;
import cn.it.ShopSystem.dao.UserDao;
import cn.it.ShopSystem.service.BaseService;

@SuppressWarnings("unchecked")
@Service("baseService")
@Lazy(true)
public class BaseServiceImpl<T> implements BaseService<T> {
	
	private Class clazz; //假设clazz中存储了当前操作的类型
	
	//实现clazz假设
	public BaseServiceImpl(){
		ParameterizedType type = (ParameterizedType)this.getClass().getGenericSuperclass();
		clazz = (Class)type.getActualTypeArguments()[0];
	}
	@PostConstruct
	public void init(){
		//根据clazz的类型，把不同的dao对象赋值给baseDao对象(反射技术)
		String clazzName = clazz.getSimpleName();
		String clazzDao = clazzName.substring(0, 1).toLowerCase() + clazzName.substring(1) + "Dao";//Account-->accountDao
		try {
			 Field clazzField = this.getClass().getSuperclass().getDeclaredField(clazzDao);
			 Field baseField = this.getClass().getSuperclass().getDeclaredField("baseDao");
			 baseField.set(this, clazzField.get(this));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	protected BaseDao baseDao;
	@Resource
	protected AccountDao accountDao;
	@Resource
	protected CategoryDao categoryDao;
	@Resource
	protected ForderDao forderDao;
	@Resource
	protected ProductDao productDao;
	@Resource
	protected SorderDao sorderDao;
	@Resource
	protected UserDao userDao;

	@Override
	public void save(T t) {
		baseDao.save(t);
	}

	@Override
	public void update(T t) {
		baseDao.update(t);
	}

	@Override
	public void delete(int id) {
		baseDao.delete(id);
	}

	@Override
	public T get(int id) {
		return (T)baseDao.get(id);
	}

	@Override
	public List<T> query() {
		return baseDao.query();
	}

}
