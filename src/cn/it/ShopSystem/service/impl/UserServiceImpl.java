package cn.it.ShopSystem.service.impl;


import org.springframework.stereotype.Service;

import cn.it.ShopSystem.model.User;
import cn.it.ShopSystem.service.UserService;
@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {
	@Override
	public User login(User user) {
		return userDao.login(user);
	}

}
