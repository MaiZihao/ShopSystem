package cn.it.ShopSystem.service.impl;

import org.springframework.stereotype.Service;

import cn.it.ShopSystem.model.Account;
import cn.it.ShopSystem.service.AccountService;
/**
 * 模块自身的业务逻辑
 * @author 麦子
 *
 */
@Service("accountService")
public class AccountServiceImpl extends BaseServiceImpl<Account> implements AccountService {
	public AccountServiceImpl(){
		super();
	}

}
