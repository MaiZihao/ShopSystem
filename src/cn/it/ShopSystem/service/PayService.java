package cn.it.ShopSystem.service;

import java.util.Map;

import cn.it.ShopSystem.model.BackData;
import cn.it.ShopSystem.model.SendData;

public interface PayService {

	// 把加密后的信息存储到requestMap中
	public abstract Map<String, Object> saveDataToRequest(
			Map<String, Object> request, SendData sendData);
	
	public abstract boolean checkBackData(BackData backData);
}