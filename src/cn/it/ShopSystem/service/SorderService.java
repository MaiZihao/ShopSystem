package cn.it.ShopSystem.service;


import java.util.List;

import cn.it.ShopSystem.model.Forder;
import cn.it.ShopSystem.model.Product;
import cn.it.ShopSystem.model.Sorder;

public interface SorderService  extends BaseService<Sorder>{
	
	//计算购物总价格
	public Forder addSorder(Forder forder,Product product);
	//根据商品编号更新商品的数量
	public Forder updateSorder(Sorder sorder,Forder forder);
	//把Product数据转化为Sorder
	public Sorder productToSorder(Product product);
	//查询热点商品的销售量
	public List<Object> querySale(int number);
}
