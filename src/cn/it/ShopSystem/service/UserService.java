package cn.it.ShopSystem.service;

import cn.it.ShopSystem.model.User;

public interface UserService  extends BaseService<User>{

	public User login(User user);
}
