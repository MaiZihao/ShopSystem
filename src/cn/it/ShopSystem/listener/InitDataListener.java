package cn.it.ShopSystem.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import cn.it.ShopSystem.model.Category;
import cn.it.ShopSystem.model.Product;
import cn.it.ShopSystem.service.CategoryService;
import cn.it.ShopSystem.service.ProductService;
import cn.it.ShopSystem.util.FileUpload;
import cn.it.ShopSystem.util.ProductTimerTask;
/**
 * 用于项目启动的时候数据的初始化
 * @author 麦子
 *
 */
public class InitDataListener implements ServletContextListener {

	private ApplicationContext  context = null;
	private ProductTimerTask productTimerTask = null;
	private FileUpload fileUpload = null;
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		//1、获取业务逻辑类查询商品信息
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		//通过工具加载相关数据
		context = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
		productTimerTask = (ProductTimerTask) context.getBean("productTimerTask");
		fileUpload = (FileUpload) context.getBean("fileUpload");

		//把内置对象交给productTimerTask
		productTimerTask.setApplication(event.getServletContext());
		//通过设置定时器，让首页的数据每隔一个小时同步一次（要配置为守护线程）
		new Timer(true).schedule(productTimerTask, 0, 1000*60*60);
		//项目启动时候要加载银行图标
		event.getServletContext().setAttribute("bankList", fileUpload.getBankImage());
	}

}
