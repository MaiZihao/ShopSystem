package cn.it.ShopSystem.util;


import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * 用来完成邮件的发送
 * @author Administrator
 *
 */
public class MassageUtilImpl {	
	
	public static void main(String[] args) throws Exception {
		//登录邮件客户端（创建回话Session）
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		//创建Session会话
		Session session = Session.getDefaultInstance(properties);
		//设置debug模式调试发送信息
		session.setDebug(true);
		//创建一封邮箱对象
		Message message = new MimeMessage(session);
		//写信
		message.setSubject("订单支付成功邮箱！");
		message.setContent("订单121541655已经支付成功！", "text/html;charset=utf-8");
		//发件人地址
		message.setFrom(new InternetAddress("soft03_test@sina.com"));
		
		Transport transport = session.getTransport();
		//链接邮件服务器的认证信息
		transport.connect("smtp.sina.com", "soft03_test", "soft03_test");
		//设置收件人地址，并且发送邮件
		transport.sendMessage(message, new Address[]{new InternetAddress("soft03_test@sina.com")});
		transport.close();
	}
}
