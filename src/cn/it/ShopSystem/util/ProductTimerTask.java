package cn.it.ShopSystem.util;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import cn.it.ShopSystem.model.Category;
import cn.it.ShopSystem.model.Product;
import cn.it.ShopSystem.service.CategoryService;
import cn.it.ShopSystem.service.ProductService;
/**
 * 设置任务：run方法中使用来加载首页信息数据
 * @author 麦子
 *
 */
@Component("productTimerTask")
public class ProductTimerTask extends TimerTask{
	@Resource
	private ProductService productService = null;
	@Resource
	private CategoryService categoryService = null;
	
	private ServletContext application = null;
	public void setApplication(ServletContext application) {
		this.application = application;
	}
	@Override
	public void run() {
		System.out.println("-----------------.run()");
		List<List<Product>> bigList = new ArrayList<List<Product>>();
		//查询出热点类别
		for (Category category : categoryService.queryByHot(true)) {
			//根据热点类别id获取推荐商品
			bigList.add(productService.queryByCid(category.getId()));
		}
		//把查询的bigList交给application内置对象
		application.setAttribute("bigList", bigList);
	}

}
