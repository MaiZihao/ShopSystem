package cn.it.ShopSystem.action;

import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.management.RuntimeErrorException;

import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.it.ShopSystem.model.Category;
import cn.it.ShopSystem.model.FileImage;
import cn.it.ShopSystem.service.AccountService;
import cn.it.ShopSystem.service.CategoryService;
import cn.it.ShopSystem.service.ForderService;
import cn.it.ShopSystem.service.ProductService;
import cn.it.ShopSystem.service.SorderService;
import cn.it.ShopSystem.service.UserService;
import cn.it.ShopSystem.service.PayService;
import cn.it.ShopSystem.util.FileUpload;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
/**
 * Struts流程：先创建Action，再调用拦截器，拦截器访问成功再调用Action的方法
 * 
 * 在项目启动的时候Struts的过滤器，已经把相应的内置对象，和内置对象相应的Map存储到了ActionContext和值栈中
 * 如果实现了相应的****Aware接口，就会从ActionContext中获取相应的Map进行传入  实现的拦截器为：servletConfig
 * servletConfig：有如下代码：判断当前实现什么接口，则会注入相应的接口
 * if(action instanceof ApplicationAware){
 * 		((ApplicationAware) action).setApplication(context.getApplication());
 * }
 * if(action instanceof SessionAware){
 * 		((SessionAware) action).setSession(context.getSession());
 * }
 * if(action instanceof RequestAware){
 * 		((RequestAware) action).setRequest(context.getRequest());
 * }
 * @author 麦子
 *
 */
@SuppressWarnings("unchecked")
@Controller
@Scope("prototype")
public class BaseAction<T> extends ActionSupport implements RequestAware,SessionAware,ApplicationAware,ModelDriven<T> {
	
	protected T model;
	
	//获取要删除的id
	protected String ids;
	
	protected InputStream inputStream;
	
	protected List<T> jsonList=null;
	
	protected Integer page;

	protected Integer rows;
	
	protected FileImage fileImage;
	
	protected Map<String, Object> pageMap = null;

	public Map<String, Object> getPageMap() {
		return pageMap;
	}	

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}
	
	public List<T> getJsonList() {
		return jsonList;
	}

	public FileImage getFileImage() {
		return fileImage;
	}

	public void setFileImage(FileImage fileImage) {
		this.fileImage = fileImage;
	}



	@Resource
	protected CategoryService categoryService;
	@Resource
	protected AccountService accountService;
	@Resource
	protected ProductService productService;
	@Resource
	protected ForderService forderService;
	@Resource
	protected SorderService sorderService;
	@Resource
	protected UserService userService;
	@Resource
	protected PayService payService;
	@Resource
	protected FileUpload fileUpload;
//	public void setAccountService(AccountService accountService) {
//		this.accountService = accountService;
//	}
//
//	public void setCategoryService(CategoryService categoryService) {
//		this.categoryService = categoryService;
//	}
//
//	public void setProductService(ProductService productService) {
//		this.productService = productService;
//	}

	
	//ModelDriven:此接口必须要实现getModel()方法，此方法会把返回的对象，压在栈项中	

	@Override
	public T getModel() {
		ParameterizedType type = (ParameterizedType)this.getClass().getGenericSuperclass();
		Class clazz = (Class)type.getActualTypeArguments()[0];
		try {
			model = (T)clazz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} 
		return model;
	}
	


	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPage() {
		return page;
	}

	public Integer getRows() {
		return rows;
	}
	
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	
	
	
	
	protected Map<String, Object> request;
	protected Map<String, Object> application;
	protected Map<String, Object> session;
	@Override
	public void setApplication(Map<String, Object> application) {
		this.application=application;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session=session;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		this.request=request;
	}

	

}
