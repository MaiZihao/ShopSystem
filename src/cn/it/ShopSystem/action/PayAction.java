package cn.it.ShopSystem.action;

import java.util.Map;

import org.apache.struts2.interceptor.ParameterAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.it.ShopSystem.model.BackData;
import cn.it.ShopSystem.model.Forder;
import cn.it.ShopSystem.model.SendData;
import cn.it.ShopSystem.model.Status;
import cn.it.ShopSystem.model.User;
/**
 * Struts处理流程：获取请求后，先创建Action代理，在创建代理的时候顺便创建了Action，执行18个拦截器，拦截器执行成功后再调用Action的方法
 * action方法执行完毕后，再返回调用拦截器
 * 
 * 创建Action-->servletConfig-->modelDriven
 * @author Administrator
 *
 */
@Controller
@Scope("prototype")
public class PayAction extends BaseAction<Object> implements ParameterAware{

	private Map<String, String[]> parameters;	
	
	
	
	@Override
	public Object getModel() {
		if(parameters.get("pd_FrpId")!=null){
			model = new SendData();
		}else {
			model = new BackData();
		}
		return model;
	}


	public void setParameters(Map<String, String[]> parameters) {
		this.parameters = parameters;
	}


	public String goBank(){
		SendData sendData = (SendData)model;
		//1.不全参数
		Forder forder = (Forder) session.get("oldForder");
		User user = (User) session.get("user");
		sendData.setP2_Order(forder.getId().toString());
		sendData.setP3_Amt(forder.getTotal().toString());
		sendData.setPa_MP(user.getEmail() +","+user.getPhone());
		//2.对参数进行追加
		//3.加密获取签名
		//4.存储到request域中
		payService.saveDataToRequest(request, sendData);
		//5.跳转到支付页面
		return "pay";
	}

	public void backBank(){
		BackData backData = (BackData)model;
		boolean isOK = payService.checkBackData(backData);
		if(isOK){
			//1.更新订单状态
			forderService.updateStatusById(2017110701, 2);
			//2.根据user邮箱地址发送邮件
			//3.发送手机短信
			
			System.out.println("PayAction.backBank(0)");
		}else {
			System.out.println("PayAction.backBank(1)");
		}
	}
	
}
