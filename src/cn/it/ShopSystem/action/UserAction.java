package cn.it.ShopSystem.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.it.ShopSystem.model.User;
@Controller
@Scope("prototype")
public class UserAction extends BaseAction<User> {

	public UserAction(){
		System.out.println("---UserAction()----");
	}
	
	public String login(){
		//进行登判断
		model = userService.login(model);
		if(model == null){
			session.put("error", "登录失败");
			return "ulogin";
		}else {
			//登录成功，先存储到session，则根据情况返回到相应页面
			session.put("user", model);
			//根据session中goURl是否有值而决定页面的跳转
			if(session.get("goURL")==null){
				return "index";
			}else {
				return "goURL";
			}
		}		
	}
	
	public String demo(){
		try {
			Integer.parseInt("xyz");
			System.out.println("---demo()---");
		} catch (Exception e) {
			throw new RuntimeException("字符转换错误！");
		}
		
		return "index";
		
	}
	
}
