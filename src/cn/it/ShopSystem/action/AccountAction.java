package cn.it.ShopSystem.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.it.ShopSystem.model.Account;
@Controller
@Scope("prototype")
public class AccountAction extends BaseAction<Account> {	
	
	public String query(){
		jsonList = accountService.query();
		return "jsonList";
	}
	
}
