package cn.it.ShopSystem.action;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.it.ShopSystem.model.Category;

import com.opensymphony.xwork2.ActionContext;
/**
 * ModelDriven:此接口必须要实现getModel()方法，此方法会把返回的对象，压在栈项中
 * @author 麦子
 *
 */
@Controller
@Scope("prototype")
public class CategoryAction extends BaseAction<Category> {	
	
	public String queryJoinAccount(){
		//用来存储分页数据
		pageMap = new HashMap<String, Object>();
		System.out.println("type:" + model.getType());
		//根据关键字和分页的参数查询相应的数据
		List<Category> categoryList = categoryService.queryJoinAccount(model.getType(), page, rows);
		pageMap.put("rows", categoryList);
		//根据关键字查询总记录数
		pageMap.put("total", categoryService.getCount(model.getType()));
		return "jsonMap";
	}
	
	public String deleteByIds(){
		System.out.println("删除id为"+ids);
		categoryService.deleteByIds(ids);
		inputStream = new ByteArrayInputStream("true".getBytes());
		return "stream";
	}
	
	public void save(){
		System.out.println(model);
		categoryService.save(model);
	}
	
	public void update(){
		System.out.println(model);
		categoryService.update(model);
	}
	
	public String query(){
		jsonList = categoryService.query();
		return "jsonList";
	}
	

}
