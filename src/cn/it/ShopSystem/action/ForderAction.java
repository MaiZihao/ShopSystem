package cn.it.ShopSystem.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


import cn.it.ShopSystem.model.Forder;
import cn.it.ShopSystem.model.Status;
import cn.it.ShopSystem.model.User;
@Controller
@Scope("prototype")
public class ForderAction extends BaseAction<Forder> {	
	
	@Override
	public Forder getModel() {
		model = (Forder) session.get("forder");
		return model;
	}

	//实现购物车（订单）与购物项（订单项） 级联入库功能
	public String save(){
//		//把session中的购物项交给当前的model对象
//		Forder forder = (Forder)session.get("forder");
//		model.setSorderSet(forder.getSorderSet());
		
		model.setUser((User)session.get("user"));
		model.setStatus(new Status(1));
		//级联入库（需要在xml中配置）,需要sorder关联forder，在SorderServiceImpl业务逻辑类中，追加sorder.setForder(forder);
		forderService.save(model);
		//此时购物车已经入库，那么原来session中的购物车应该清空
		session.put("oldForder", session.remove("forder"));//把购物车存储起来为了后面支付可以使用相关信息
		//session.put("forder", new Forder());//新的空购物车
		
		return "bank";
	}
}
