package cn.it.ShopSystem.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 此Action用来完成web-inf中jsp与jsp请求转发功能，此Action不处理任何的逻辑
 * @author 麦子
 *
 */
@Controller
public class SendAction extends ActionSupport {
	
	public SendAction() {
		System.out.println("--sendAction--");
	}
	
	public String execute(){
		
		return "send";
	}
}
