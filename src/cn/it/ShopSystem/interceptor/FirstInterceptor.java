package cn.it.ShopSystem.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class FirstInterceptor implements Interceptor{
	
	public FirstInterceptor(){
		System.out.println("----FirstInterceptor---");
	}
	
	@Override
	public void destroy() {
		
	}

	@Override
	public void init() {
		System.out.println("-----First init()----");
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		System.out.println("-----First in -------");
		String result = invocation.invoke();
		System.out.println("result:"+result);
		System.out.println("-----First out -------");
		return result;
	}

}
